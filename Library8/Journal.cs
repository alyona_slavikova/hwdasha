﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library8
{
    class Journal
    {
       
        public string Name;
        public int indexOfBook;
        public int Days;
    
//создаем список журнала
     public List<Journal> journal = new List<Journal>();

     public Journal(int booksIndex, string name,  int days)
        {
            indexOfBook = booksIndex;
            Name = name;
            Days = days;
        }
        public Journal() { } //пустой констуктор нужен чтоб обращаться к методу
        AskInfo information = new AskInfo();
        /// Добавляет записи в журнал.

        public List<Journal> AddInformationToJournal(List<Books> books, List<Journal> journal)
        {

            journal.Add(new Journal(information.IndexOfBookToRead(books), information.ReaderName(), information.AmountOfDays()));
            return journal;
        }

        /// Выводит каждую запись в журнале.
         public void ShowJournal(List<Journal> books)
        {
            //Console.Clear();
            foreach (Journal data in journal)
            { // @ нужна чтоб вывести информацию именно так как написано в visual Studio (переход строки, абзацы, отступы.)
               Console.WriteLine($@"Name: {data.Name}
                 Title: {books[data.indexOfBook - 1].Name}
                Will be returned in : {data.Days} days");
            }
        }
        /// Выводит название книги, автора книги, сколько всего дней все книги были у всех читателей.
        
        public void ShowStatistics(List<Books> books, List<Journal> journal)
        {
            int totalNumberOfDays = 0;
            Console.Clear();
            Console.WriteLine("Book that is being read: ");
            int count = 1;
            foreach (var data in journal)
            {
                totalNumberOfDays += data.Days;
                Console.WriteLine($"{count} -\nTitle: {books[data.indexOfBook - 1].Name}");
                Console.WriteLine();
                count++;
            }
            Console.WriteLine("Total number of days that readers had a book: " + totalNumberOfDays + "\n");
        }
       
    }
}
