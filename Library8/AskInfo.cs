﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library8
{
    class AskInfo
    {
       
        public int NumberOfBook(List<Books> books)
        {
            int Choise;
            bool CorrectInput = false;
            do
            {
                Console.WriteLine("Choose the number of the book");
                CorrectInput = int.TryParse(Console.ReadLine(), out Choise);//out - если то что считано число - присвой то что ввели переменной choise в виде числа
                if (CorrectInput == true && Choise > 0 && Choise <= books.Count)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Your choise is wrong!");
                }
            } while (true);
            return Choise;
        }

        public string AskAuthorOfBook()
        {
            Console.Clear();
            Console.WriteLine("Enter an author of the book: ");
            return Console.ReadLine();
        }

        public string ReaderName()
        {
            Console.Clear();
            Console.WriteLine("Enter your name: ");
            return Console.ReadLine();
        }
        public int AmountOfDays()
        {
            Console.Clear();
            Console.WriteLine("How long will your read the book: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public int IndexOfBookToRead(List<Books> books)
        {
            Console.Clear();
            Console.WriteLine("Enter index of the book: ");
            return Convert.ToInt32(Console.ReadLine());
        }

    }
}