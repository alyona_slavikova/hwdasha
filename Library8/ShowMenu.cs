﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



    namespace Library8
    {
        public class ShowMenu
        {
            public string AskMainMenu()
            {
                Console.WriteLine("Select an option from 1 to 9");
                Console.WriteLine("1 - show books from the catalog");
                Console.WriteLine("2 - add  book to the catalog");
                Console.WriteLine("3 - delete book from catalog");
                Console.WriteLine("4 - show books list alphabetically by author");
                Console.WriteLine("5 - show books list alphabetically by name");
                Console.WriteLine("6 - show books of specific author");
                Console.WriteLine("7 - take a book");
                Console.WriteLine("8 - show statistics");
                Console.WriteLine("9 - if you want to  exit this program");
                return Console.ReadLine();

            }


            public string AskBooksName()
            {
                Console.WriteLine("Please enter the name of the  book: ");
                return Console.ReadLine();
            }

            public string AskBooksAuthor()
            {
                Console.WriteLine("Please enter the author of the book: ");
                return Console.ReadLine();
            }

            public void ShowInvalidMenuText()
            {
                Console.WriteLine("You chose wrong option.");
            }
        }
    }
