﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library8
{
    public class Library
    {
        ShowMenu menu = new ShowMenu();
        //список книг
        public List<Books> books = new List<Books>();
        public List<Books> BooksInLibrary(List<Books>books)
        {
           
            books.Add (new Books("Anna Karenina", "Leo Tolstoy"));
            books.Add(new Books("Lolita", "Vladimir Nabokov"));
            books.Add(new Books("Don Quixote", "Miguel de Cervantes"));
            books.Add(new Books("The Great Gatsby", "F. Scott Fitzgerald"));
            books.Add(new Books("Hamlet", "William Shakespeare"));
            return books;
        }


        public void AddBook(string name, string author, List<Books> booksAdd)
        {

            booksAdd.Add(new Books(name, author));

        }
        public void ShowAllBooks(List<Books> books)
        {
            foreach(Books element in books)
            {
            
                {
                    Console.WriteLine(element.Name + "-" + element.Author);
                }
            
            }
            Console.WriteLine();
        }

        public void DeleteBook(List<Books> books, int index)
        {
            books.RemoveAt(index-1);
        }

        //Метод сортировки по названию книги
        public void SortbyName(List<Books> books)
        {// разделяет на слова по пробелу, отделить 0 элемнт в предложении и по нему сортируй, и сохрани в существующий лист
            Console.Clear();
            Console.WriteLine("The contents of the catalog alphabetically by the name of the book");
            books = books.OrderBy(item => item.Name.Split(' ').ElementAtOrDefault(0)).ToList();
            foreach (Books element in books)
            {
                Console.WriteLine($" Name of book: {element.Name}");
            }
            Console.WriteLine();
        }
        //Метод сортировки по автору книги
        public void SortByAuthor(List<Books> books)
        {
            Console.Clear();
            Console.WriteLine("The contents of the catalog alphabetically by the author of the book");
            books = books.OrderBy(item => item.Author.Split(' ').ElementAtOrDefault(0)).ToList();
            foreach (Books element in books)
            {
                Console.WriteLine($" Autor: {element.Author}");
            }
            Console.WriteLine();
        }

        public void ShowBooksBySpecificAuthor(List<Books> books, string author)
        {
            Console.Clear();
            Console.WriteLine("Books by " + author );
            int count = 0;
            foreach (var book in books)
            {
                if (book.Author.Contains(author))
                {
                    count++;
                    Console.WriteLine(book.Name + "-" + book.Author);
                }
            }
            if (count == 0)
            {
                menu.ShowInvalidMenuText();
            }
            Console.WriteLine();
        }

    }
}
