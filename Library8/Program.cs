﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library8
{
    class Program
    {
        public static void Main(string[] args)
        {
            Library library = new Library();
            ShowMenu menu = new ShowMenu();
            AskInfo information = new AskInfo();
            Journal journal = new Journal();
            library.BooksInLibrary(library.books);

            while (true)
            {
                string menuPoint = menu.AskMainMenu(); //выбираем изначально что хотим сделать
                switch (menuPoint)
                {
                    case "1"://показать список книг
                        
                        library.ShowAllBooks(library.books);
                        
                        break;
                    case "2"://добавить книгу в каталог
                        //записывается название книги через метод
                        library.AddBook(menu.AskBooksName(), menu.AskBooksAuthor(), library.books);
                        library.ShowAllBooks(library.books);
                        
                        break;

                    case "3"://исключить книгу из каталога
                        library.ShowAllBooks(library.books);
                        library.DeleteBook(library.books, information.NumberOfBook(library.books));
                        break;

                    case "4"://вывести в алфавитном порядке по автору

                        library.ShowAllBooks(library.books);
                        library.SortByAuthor(library.books);
                        break;

                    case "5"://вывести в алфавитном порядке по названию
                        library.ShowAllBooks(library.books);
       
                        library.SortbyName(library.books);
                        break;

                    case "6"://вывести книгу по автору
                        library.ShowBooksBySpecificAuthor(library.books, information.AskAuthorOfBook());
                        break;
                    case "7"://взять книгу и добавить в журнал
                        library.ShowAllBooks(library.books);
                        journal.AddInformationToJournal(library.books,journal.journal);
                        break;
                      
                    case "8"://показать статистику
                        journal.ShowStatistics(library.books, journal.journal);
                        break;
                    case "9"://выйти из программы
                        return;
                    default:
                        menu.ShowInvalidMenuText();
                        break;

                }
            }

            
        }
    }
}
